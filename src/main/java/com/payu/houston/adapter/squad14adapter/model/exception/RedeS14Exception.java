package com.payu.houston.adapter.squad14adapter.model.exception;

/**
 * The Class RedeS14Exception.
 *
 * @author <a href = "carlos.maturana@payulatam.com">Carlos Maturana</a>
 * @version 1.0
 */
public class RedeS14Exception extends RuntimeException {

	/**
	 * Serialization UID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create a RedeS14 runtime exception with a message
	 * @param message
	 */
	public RedeS14Exception(final String message) {
		super(message);
	}

	/**
	 * Create a RedeS14 runtime exception with a message and throwable
	 * @param message
	 * @param throwable
	 */
	public RedeS14Exception(final String message, final Throwable throwable) {
		super(message, throwable);
	}

	/**
	 * Create a RedeS14 runtime exception with a throwable
	 * @param throwable
	 */
	public RedeS14Exception(final Throwable throwable) {
		super(throwable);
	}


}
