package com.payu.houston.adapter.squad14adapter.model.authorization;

import com.payu.houston.adapter.queue.model.common.ResponseMessage;

import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;

public class AbstractAuthorizationResponse extends ResponseMessage implements Serializable {

	/**
	 * UID
	 */
	private static final long serialVersionUID = 1L;

	@XmlElement(name = "codRet", required = true)
	private String codRet;

	@XmlElement(name = "msgret", required = true)
	private String msgret;

	@XmlElement(name = "data", required = true)
	private String data;

	@XmlElement(name = "hora", required = true)
	private String hora;

	@XmlElement(name = "numPedido", required = true)
	private String numPedido;

	@XmlElement(name = "numAutor", required = true)
	private String numAutor;

	@XmlElement(name = "numSqn", required = true)
	private String numSqn;

	@XmlElement(name = "tid", required = true)
	private String tid;


	@XmlElement(name = "codRet", required = true)
	public String getCodRet() {

		return codRet;
	}

	public void setCodRet(String codRet) {

		this.codRet = codRet;
	}

	@XmlElement(name = "msgret", required = true)
	public String getMsgret() {

		return msgret;
	}

	public void setMsgret(String msgret) {

		this.msgret = msgret;
	}

	@XmlElement(name = "data", required = true)
	public String getData() {

		return data;
	}

	public void setData(String data) {

		this.data = data;
	}

	@XmlElement(name = "hora", required = true)
	public String getHora() {

		return hora;
	}

	public void setHora(String hora) {

		this.hora = hora;
	}

	@XmlElement(name = "numPedido", required = true)
	public String getNumPedido() {

		return numPedido;
	}

	public void setNumPedido(String numPedido) {

		this.numPedido = numPedido;
	}

	@XmlElement(name = "numAutor", required = true)
	public String getNumAutor() {

		return numAutor;
	}

	public void setNumAutor(String numAutor) {

		this.numAutor = numAutor;
	}

	@XmlElement(name = "numSqn", required = true)
	public String getNumSqn() {

		return numSqn;
	}

	public void setNumSqn(String numSqn) {

		this.numSqn = numSqn;
	}

	@XmlElement(name = "tid           s", required = true)
	public String getTid() {

		return tid;
	}

	public void setTid(String tid) {

		this.tid = tid;
	}
}
