package com.payu.houston.adapter.squad14adapter.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

// import TDES.TDESBase64;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class EncryptionUtil {
	/** The tool used to Encrypt */
	private final BASE64Encoder encryptionTool;

	/** The tool used to Decrypt */
	private final BASE64Decoder decryptionTool;

	/** The Constant encryptionKey. */
	private static final Map<String, EncryptionUtil> encryptionKey = new ConcurrentHashMap<>();

	/**
	 * Default constructor
	 *
	 * @param key
	 */
	protected EncryptionUtil(final String key) {

		encryptionTool = new BASE64Encoder();
		decryptionTool = new BASE64Decoder();
	}

	/**
	 * Only method to access this singleton reference
	 *
	 * @param newKey
	 * @return
	 * @author <a href=camilo.white@payulatam.com>Camilo A. White</a>
	 */
	public static EncryptionUtil getInstance(final String newKey) {

		Validate.notNull(newKey, "The key may not be null");
		if(!encryptionKey.containsKey(newKey)){
			encryptionKey.put(newKey, new EncryptionUtil(newKey));
		}
		return encryptionKey.get(newKey);

	}

	/**
	 * Encrypts the data with 3 DES 64 bits
	 *
	 * @param data
	 * @return
	 * @author <a href=camilo.white@payulatam.com>Camilo A. White</a>
	 */
	public String encryptData(final String data) {

		synchronized (encryptionTool) {

			// return encryptionTool.Encripta(data);
			return encryptionTool.encode(data.getBytes());
		}
	}

	/**
	 * Decrypts the data with 3 DES 64 bits
	 *
	 * @param data
	 * @return descryptData or EMTY if data is present
	 * @throws UnsupportedEncodingException
	 * @author <a href=camilo.white@payulatam.com>Camilo A. White</a>
	 */
	public String decryptData(final String data) throws UnsupportedEncodingException, IOException {

		synchronized (decryptionTool) {
			// final String decryptData = encryptionTool.dec(encryptionTool.DecodingBase64(data));
			final String decryptData = String.valueOf(decryptionTool.decodeBuffer(data));
			return StringUtils.isNoneBlank(decryptData) ? decryptData : StringUtils.EMPTY;
		}

	}
}

