package com.payu.houston.adapter.squad14adapter.service;

import com.payu.houston.adapter.AbstractEventLogger;
import com.payu.houston.adapter.squad14adapter.model.authorization.AbstractAuthorizationRequest;
import com.payu.houston.adapter.squad14adapter.model.authorization.AbstractAuthorizationResponse;
import com.payu.logger.logevents.service.LogEventAppenderService;
import com.payu.logger.logevents.util.LogEventUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class EventLoggerService extends AbstractEventLogger<AbstractAuthorizationRequest, AbstractAuthorizationResponse> {

	@Autowired
	protected EventLoggerService(final LogEventAppenderService logEventAppender) {

		super(logEventAppender);
	}

	@Override protected Map<LogEventUtil.MaskType, List<String>> initFieldsToMask() {

		return null;
	}

	@Override protected String getPaymentNetworkName() {

		return "RedeS14";
	}

	public void doSaveResponseMessage() {

	}
}
