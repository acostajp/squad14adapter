package com.payu.houston.adapter.squad14adapter.util;

import com.payu.houston.adapter.queue.model.authorization.AuthorizationRequestMessage;

import java.text.SimpleDateFormat;
import java.util.Date;

public class RedeUtil {

	private RedeUtil(){

	}

	/**Date format for Rede*/
	private  static  final String DATA_FORMAT="YYYYMMDD";

	/**Time formate for Rede*/
	private static  final String TIME_FORMAT="HH:MM:SS";

	private static  final String getDataFormat(final Date date){
		return new SimpleDateFormat(DATA_FORMAT).format(date);
	}

	private static  final String getTimeFormat(final Date date){
		return new SimpleDateFormat(TIME_FORMAT).format(date);
	}

	public static String buildEncryptKey(AuthorizationRequestMessage requestMessage) {
		return null;

	}
}
