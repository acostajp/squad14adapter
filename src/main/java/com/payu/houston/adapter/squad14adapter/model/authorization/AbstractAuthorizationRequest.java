package com.payu.houston.adapter.squad14adapter.model.authorization;

import com.payu.houston.adapter.queue.model.common.RequestMessage;
import com.payu.houston.adapter.squad14adapter.model.exception.RedeS14Exception;
import com.payu.houston.adapter.squad14adapter.utils.RedeS14Util;
import org.apache.commons.lang.StringUtils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Authorization")
public class AbstractAuthorizationRequest extends RequestMessage implements Serializable {

	/** Year of validity of the card */
	@XmlElement(name = "ano", required = true)
	private String ano;

	/** Card security code */
	@XmlElement(name = "cvc2", required = false)
	private String cvc2;

	/** Membership number of the establishment  */
	@XmlElement(name = "filiacao", required = true)
	private String filiacao;

	/** Month of validity of the card */
	@XmlElement(name = "mes", required = true)
	private String mes;

	/** Card number */
	@XmlElement(name = "nrcartao", required = true)
	private String nrcartao;

	/** Order number */
	@XmlElement(name = "numPedido", required = true)
	private String numPedido;

	/** the transaction origin. */
	@XmlElement(name = "origem", required = true)
	private String origem;

	/** Number of installments */
	@XmlElement(name = "parcelas", required = false)
	private String parcelas;

	/** Cardholder ́s name */
	@XmlElement(name = "portador", required = false)
	private String portador;

	/** if  the  transaction  is  a recurrence */
	@XmlElement(name = "recorrente", required = true)
	private String recorrente;

	/** Token authentication in e.Rede API */
	@XmlElement(name = "senha", required = true)
	private String senha;

	/** Total purchase value */
	@XmlElement(name = "total", required = true)
	private String total;

	/** Transaction type */
	@XmlElement(name = "transacao", required = true)
	private String transacao;

	public AbstractAuthorizationRequest() {
		// Empty constructor
	}

	public AbstractAuthorizationRequest(Builder builder) {

		this.ano = builder.ano;
		this.cvc2 = builder.cvc2;
		this.filiacao = builder.filiacao;
		this.mes = builder.mes;
		this.nrcartao = builder.nrcartao;
		this.numPedido = builder.numPedido;
		this.origem = builder.origem;
		this.parcelas = builder.parcelas;
		this.portador = builder.portador;
		this.recorrente = builder.recorrente;
		this.senha = builder.senha;
		this.total = builder.total;
		this.transacao = builder.transacao;
	}

	/**
	 * Creates new Builder with encryption key.
	 *
	 * @return the builder
	 */
	public static Builder create(String encryptKey) {
		return new Builder(encryptKey);
	}

	/**
	 * Gets the year.
	 *
	 * @return the ano
	 */
	@XmlElement(name = "ano", required = true)
	public String getAno() {

		return ano;
	}

	public void setAno(String ano) {

		this.ano = ano;
	}

	/**
	 * Gets the cvc2.
	 *
	 * @return the cvc2
	 */
	@XmlElement(name = "cvc2", required = false)
	public String getCvc2() {

		return cvc2;
	}

	public void setCvc2(String cvc2) {

		this.cvc2 = cvc2;
	}

	/**
	 * Gets the Merchant Id.
	 *
	 * @return the filiacao
	 */
	@XmlElement(name = "filiacao", required = true)
	public String getFiliacao() {

		return filiacao;
	}

	public void setFiliacao(String filiacao) {

		this.filiacao = filiacao;
	}

	/**
	 * Gets the Month.
	 *
	 * @return the mes
	 */
	@XmlElement(name = "mes", required = true)
	public String getMes() {

		return mes;
	}

	public void setMes(String mes) {

		this.mes = mes;
	}

	/**
	 * Gets the Credit Card Number.
	 *
	 * @return the nrcartao
	 */
	@XmlElement(name = "nrcartao", required = true)
	public String getNrcartao() {

		return nrcartao;
	}

	public void setNrcartao(String nrcartao) {

		this.nrcartao = nrcartao;
	}

	/**
	 * Gets the Order Number.
	 *
	 * @return the numPedido
	 */
	@XmlElement(name = "numPedido", required = true)
	public String getNumPedido() {

		return numPedido;
	}

	public void setNumPedido(String numPedido) {

		this.numPedido = numPedido;
	}

	/**
	 * Gets the Transaction Origin.
	 *
	 * @return the origem
	 */
	@XmlElement(name = "origem", required = true)
	public String getOrigem() {

		return origem;
	}

	public void setOrigem(String origem) {

		this.origem = origem;
	}

	/**
	 * Gets the Installments number.
	 *
	 * @return the parcelas
	 */
	@XmlElement(name = "parcelas", required = false)
	public String getParcelas() {

		return parcelas;
	}

	public void setParcelas(String parcelas) {

		if (parcelas != null && parcelas.length() != 2) {
			throw new RedeS14Exception("Installments format incorrect");
		}
		this.parcelas = parcelas;
	}

	/**
	 * Gets the Credit card owner name.
	 *
	 * @return the portador
	 */
	@XmlElement(name = "portador", required = false)
	public String getPortador() {

		return portador;
	}

	public void setPortador(String portador) {

		this.portador = portador;
	}

	/**
	 * Gets if the transaction is recurrent.
	 *
	 * @return the recorrente
	 */
	@XmlElement(name = "recorrente", required = true)
	public String getRecorrente() {

		return recorrente;
	}

	public void setRecorrente(String recorrente) {

		this.recorrente = recorrente;
	}

	/**
	 * Gets the token of the transaction.
	 *
	 * @return the senha
	 */
	@XmlElement(name = "senha", required = true)
	public String getSenha() {

		return senha;
	}

	public void setSenha(String senha) {

		this.senha = senha;
	}

	/**
	 * Gets the Total amount of the transaction.
	 *
	 * @return the total
	 */
	@XmlElement(name = "Total", required = true)
	public String getTotal() {

		return total;
	}

	public void setTotal(String total) {

		this.total = total;
	}

	/**
	 * Gets the Transaction type.
	 *
	 * @return the transacao
	 */
	@XmlElement(name = "transacao", required = true)
	public String getTransacao() {

		return transacao;
	}

	public void setTransacao(String transacao) {

		if (transacao == null) {
			throw new RedeS14Exception("RedeS14 process type field transaction can not be null!");
		}
		this.transacao = transacao;
	}

	public static class Builder {

		private String ano;

		private String cvc2;

		private String filiacao;

		private String mes;

		private String nrcartao;

		private String numPedido;

		private String origem;

		private String parcelas;

		private String portador;

		private String recorrente;

		private String senha = StringUtils.EMPTY;

		private String total;

		private String transacao;

		private String encryptKey;

		private Builder(String encryptKey) {
			this.encryptKey = encryptKey;
		}

		private Builder withAno(String ano) {
			this.ano = RedeS14Util.encryptData(encryptKey, ano);;
			return this;
		}

		private Builder withCVC2(String cvc2) {
			this.cvc2 = RedeS14Util.encryptData(encryptKey, cvc2);
			return this;
		}

		private Builder withFiliacao(String filiacao) {
			this.filiacao = filiacao;
			return this;
		}

		private Builder withMes(String mes) {
			this.mes = RedeS14Util.encryptData(encryptKey, mes);
			return this;
		}

		private Builder withNrcartao(String nrcartao) {
			this.nrcartao = nrcartao;
			return this;
		}

		private Builder withNumPedido(String numPedido) {
			this.numPedido = numPedido;
			return this;
		}

		private Builder withOrigem(String origem) {
			this.origem = origem;
			return this;
		}

		private Builder withParcelas(String parcelas) {
			this.parcelas = parcelas;
			return this;
		}

		private Builder withPortador(String portador) {
			this.portador = portador;
			return this;
		}

		private Builder withRecorrente(String recorrente) {
			this.recorrente = recorrente;
			return this;
		}

		private Builder withSenha(String senha) {
			this.senha = senha;
			return this;
		}

		private Builder withTotal(BigDecimal total) {
			this.total = RedeS14Util.encryptData(encryptKey, RedeS14Util.formatAmount(total));
			return this;
		}

		private Builder withTransacao(String transacao) {
			if (transacao == null) {
				throw new RedeS14Exception("RedeS14 type field transacao can not be null!");
			}
			this.transacao = transacao;
			return this;
		}
	}
}

