package com.payu.houston.adapter.squad14adapter.provider;

import com.payu.houston.adapter.AdapterProvider;
import com.payu.houston.adapter.clients.AlexandriaClient;
import com.payu.houston.adapter.clients.UtsClient;
import com.payu.houston.adapter.domain.Account;
import com.payu.houston.adapter.domain.transactional.CreditCard;
import com.payu.houston.adapter.queue.model.authorization.AuthorizationRequestMessage;
import com.payu.houston.adapter.queue.model.authorization.AuthorizationResponseMessage;
import com.payu.houston.adapter.queue.model.capture.CaptureRequestMessage;
import com.payu.houston.adapter.queue.model.capture.CaptureResponseMessage;
import com.payu.houston.adapter.queue.model.query.QueryRequestMessage;
import com.payu.houston.adapter.queue.model.query.QueryResponseMessage;
import com.payu.houston.adapter.queue.model.refunds.RefundRequestMessage;
import com.payu.houston.adapter.queue.model.refunds.RefundResponseMessage;
import com.payu.houston.adapter.queue.model.voids.VoidRequestMessage;
import com.payu.houston.adapter.queue.model.voids.VoidResponseMessage;
import com.payu.houston.adapter.squad14adapter.client.AuthorizationClient;
import com.payu.houston.adapter.squad14adapter.model.authorization.AbstractAuthorizationRequest;
import com.payu.houston.adapter.squad14adapter.model.authorization.AbstractAuthorizationResponse;
import com.payu.houston.adapter.squad14adapter.service.EventLoggerService;
import com.payu.houston.adapter.squad14adapter.util.RedeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;


import java.util.Date;

import static com.payu.houston.adapter.domain.broker.PaymentInstrumentParameters.CARD_TOKEN;

@Component
public class RedeS14Provider implements AdapterProvider {
	private  static final Logger LOGGER= LoggerFactory.getLogger(RedeS14Provider.class);

	/** The uts client. */
	//@Autowired
	private UtsClient utsClient;

	/** The Alexandiria client. */
	private AlexandriaClient alexandriaClient;

	/** Event logger service */
	private EventLoggerService eventLogger;
	/** Message validator services*/
	private MessageValidator messageValidator;

	/***/
	private ApplicationContext applicationContext;

	public RedeS14Provider() {
	}


	//TO DO: Create a new constructor to inyect uts, eventlogget and alexandria services
	@Autowired
	public RedeS14Provider(final EventLoggerService eventLogger, final MessageValidator messageValidator) {
		//this.utsClient=utsClient;
		this.messageValidator=messageValidator;
		this.eventLogger = eventLogger;
	}

	/**
	 *
	 * @param requestMessage
	 * @return
	 */
	@Override public AuthorizationResponseMessage doAuthorization(AuthorizationRequestMessage requestMessage) {
		LOGGER.info("Processing authorizattion request for REDE with AuthorizationID [{}] and CorrelationID[{}]",
					requestMessage.getAuthorizationId(),requestMessage.getCorrelationId());
		final AuthorizationResponseMessage.Builder responseBuilder= AuthorizationResponseMessage
				.withCorrelationId(requestMessage.getCorrelationId());
		try{
			messageValidator.doValidateParameterForauthorizationRequest(requestMessage);
			Date currentDate = new Date();
			Account account= findAccountById(requestMessage.getMerchant().getAccountId());
			CreditCard creditCard=getCreditCard(requestMessage.getPaymentInstrument().get(CARD_TOKEN.name()));
			String encryptKey= RedeUtil.buildEncryptKey(requestMessage);
			//final AbstractAuthorizationRequest authorizationRequest=AuthorizationClient.createAuthorizationRequest(requestMessage, currentDate, account, encryptKey);
			//eventLogger.doSaveRequestMessge(authorizationRequest, requestMessage,false);
			//final AbstractAuthorizationResponse authorizationResponse=AuthorizationClinet.doAuhtorization(authorizationRequest);
			//transalete method of authorization client
			//eventLogger.doSaveResponseMessage(authorizationResponse,requestMessage,false);


		}
		catch (final RuntimeException e){

		}
		AuthorizationResponseMessage authorizationResponseMessage=responseBuilder.build();
		LOGGER.info("Authorization process sucessfull ");
		return authorizationResponseMessage;
	}

	/**
	 *
	 * @param requestMessage
	 * @return
	 */
	@Override public CaptureResponseMessage doCapture(CaptureRequestMessage requestMessage) {

		return null;
	}

	/**
	 *
	 * @param requestMessage
	 * @return
	 */
	@Override public RefundResponseMessage doRefund(RefundRequestMessage requestMessage) {

		return null;
	}

	/**
	 *
	 * @param requestMessage
	 * @return
	 */
	@Override public VoidResponseMessage doVoid(VoidRequestMessage requestMessage) {

		return null;
	}

	/**
	 *
	 * @param requestMessage
	 * @return
	 */
	@Override public QueryResponseMessage doQuery(QueryRequestMessage requestMessage) {

		return null;
	}

	/**
	 *
	 * @param cardTocken
	 * @return
	 */
	private CreditCard getCreditCard(String cardTocken){
		return utsClient.getToken(cardTocken)
						.orElseThrow(() -> new IllegalArgumentException(String.format("Credit card. Not found with Token id: [%s].", cardTocken)));
	}

	/**
	 *
	 * @param accountId
	 * @return
	 */
	private Account findAccountById(Integer accountId){
		return alexandriaClient.getAccount(accountId)
							   .orElseThrow(() -> new IllegalArgumentException(String.format("Invalid account. Not found with id: [%s].", accountId)));
	}

	/**
	 *
	 * @return
	 */
	protected AuthorizationClient prepareConnection(){
		final AuthorizationClient authorizationClient=(AuthorizationClient)applicationContext.getBean("AuthorizationClient");
		//client method to inizialite connnection
		return authorizationClient;
	}

}
