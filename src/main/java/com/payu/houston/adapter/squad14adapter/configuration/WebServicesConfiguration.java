package com.payu.houston.adapter.squad14adapter.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.jaxws.JaxWsPortProxyFactoryBean;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Configures the REDE WebServices.
 *
 * @author <a href="carlos.maturana@payulatam.com">Carlos Maturana</a>
 * @since 1.0.0
 */
@Configuration
public class WebServicesConfiguration {

	/** The Constant JAVAX_XML_WS_SPI_PROVIDER. */
	private static final String JAVAX_XML_WS_SPI_PROVIDER = "javax.xml.ws.spi.Provider";

	/** The Constant COM_SUN_XML_INTERNAL_WS_SPI_PROVIDER_IMPL. */
	private static final String COM_SUN_XML_INTERNAL_WS_SPI_PROVIDER_IMPL = "com.sun.xml.internal.ws.spi.ProviderImpl";

	/**
	 * REDE service.
	 *
	 * @return the jax ws port proxy factory bean
	 * @throws MalformedURLException the malformed URL exception
	 */
	@Bean
	public JaxWsPortProxyFactoryBean procesosMcService() throws MalformedURLException {
		JaxWsPortProxyFactoryBean jaxWsPortProxyFactoryBean = new JaxWsPortProxyFactoryBean();
		jaxWsPortProxyFactoryBean.setServiceInterface(IKomerciWcf.class);
		jaxWsPortProxyFactoryBean.setWsdlDocumentUrl(new URL("classpath:/META-INF/wsdl/KomerciWcf.wsdl"));
		jaxWsPortProxyFactoryBean.setEndpointAddress("https://scommerce.userede.com.br/Redecard.Komerci.External.WcfKomerci/KomerciWcf.svc");
		jaxWsPortProxyFactoryBean.setNamespaceUri("http://tempuri.org/");
		jaxWsPortProxyFactoryBean.setServiceName("komerciwcf");
		jaxWsPortProxyFactoryBean.setLookupServiceOnStartup(false);
		jaxWsPortProxyFactoryBean.addCustomProperty(JAVAX_XML_WS_SPI_PROVIDER, COM_SUN_XML_INTERNAL_WS_SPI_PROVIDER_IMPL);
		return jaxWsPortProxyFactoryBean;
	}
}
