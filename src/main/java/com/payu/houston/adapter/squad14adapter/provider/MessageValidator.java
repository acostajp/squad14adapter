package com.payu.houston.adapter.squad14adapter.provider;

import com.payu.houston.adapter.queue.model.authorization.AuthorizationRequestMessage;
import com.payu.houston.adapter.queue.model.authorization.AuthorizationResponseMessage;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.stereotype.Component;
import org.apache.commons.lang3.Validate;

import static com.payu.houston.adapter.domain.broker.PaymentInstrumentParameters.CARD_TOKEN;
import static com.payu.houston.adapter.domain.broker.PaymentInstrumentParameters.PAYMENT_BRAND;
import static com.payu.houston.adapter.domain.broker.PaymentInstrumentParameters.PARENT_TYPE;

import javax.validation.Valid;
import javax.xml.bind.ValidationEvent;

@Component
public class MessageValidator {

	/**
	 *
	 * @param requestMessage
	 */
	public  void doValidateParameterForauthorizationRequest(final AuthorizationRequestMessage requestMessage){
		try{
			Validate.notNull(requestMessage,"authorization message must not be null");
			validateAmount(requestMessage);
			validatecommonParametersDetails(requestMessage);
			validatePaymentInstruments(requestMessage);
		}
		catch (RuntimeException e){
			throw  new IllegalArgumentException("Inavalid arguments for request message",e);
		}
	}

	/**
	 *
	 * @param requestMessage
	 */
	private void validateAmount(AuthorizationRequestMessage requestMessage){
			Validate.notNull(requestMessage.getAmount(),"Amount value must not be null");
			Validate.notNull(requestMessage.getAmount().getTotal(),"Total value must not be null");
			Validate.notNull(requestMessage.getAmount().getCurrency(),"Currency value must not be null");
	}

	/**
	 *
	 * @param requestMessage
	 */

	private void validatecommonParametersDetails(AuthorizationRequestMessage requestMessage){
		Validate.notNull(requestMessage.getRoute(),"Rede must have a route");

	}

	/**
	 *
	 * @param requestMessage
	 */
	private void validatePaymentInstruments(AuthorizationRequestMessage requestMessage){
		Validate.notNull(requestMessage.getPaymentInstrument(),"Request must have payment Intsruments");
		Validate.notNull(requestMessage.getPaymentInstrument().get(PAYMENT_BRAND.name()),"Payment brand is requerid");
		Validate.notNull(requestMessage.getPaymentInstrument().get(PARENT_TYPE.name()),"Payment type is required");
		Validate.notNull(requestMessage.getPaymentInstrument().get(CARD_TOKEN.name()),"Credit card token is required");
	}
}
