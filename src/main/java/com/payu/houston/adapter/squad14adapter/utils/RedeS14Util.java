package com.payu.houston.adapter.squad14adapter.utils;

import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;

/**
 * The Class RedeS14Exception.
 *
 * @author <a href = "carlos.maturana@payulatam.com">Carlos Maturana</a>
 * @version 1.0
 */
public class RedeS14Util {
	
	/** The date format for RedeS14 */
	private static final String DATE_FORMAT = "yyyyMMdd";

	/** The time format for RedeS14 */
	private static final String TIME_FORMAT = "HHmmss";

	/** The decimal places for total value */
	private static final int TOTAL_DECIMAL_PLACES = 2;

	/** The max length of the I2 field. */
	private static final int MAX_LENGTH_FOR_I2_FIELD = 15;

	/** The GUID separator character. */
	private static final String GUID_SEPARATOR = "-";

	/** Default value for the soft descriptor */
	private static final String SOFT_DESCRIPTOR_DEFAULT = "PayU";


	/**
	 * Default private constructor of this class
	 */
	private RedeS14Util() {
		// There are no fields needed to initiate
	}

	/**
	 * Format the amount, from {@link BigDecimal} to {@link String}, following the
	 * rules provided by RedeS14.
	 *
	 * @param amount the {@link BigDecimal} amount.
	 * @return the {@link String} total.
	 * @author <a href=carlos.maturana@payulatam.com>Carlos Maturana</a>
	 */
	public static String formatAmount(final BigDecimal amount) {

		if (amount == null) {
			throw new IllegalArgumentException("the amount may not be null");
		}

		final BigDecimal formatTotal = amount.setScale(TOTAL_DECIMAL_PLACES, BigDecimal.ROUND_HALF_UP);
		return formatTotal.toString();
	}

	/**
	 * Format the order number using the orderId and the transactionId.
	 *
	 * @param orderId       The orderId.
	 * @param transactionId The transactionId.
	 * @return The orderNumber for proceso mc.
	 */
	public static String formatOrderNumber(final Integer orderId, final String transactionId) {
		StringBuilder result = new StringBuilder();
		int orderIdLength = String.valueOf(orderId).length();
		result.append(String.format("%02d", orderIdLength));
		result.append(orderId);
		result.append(transactionId.replace(GUID_SEPARATOR,
											StringUtils.EMPTY).subSequence(0, MAX_LENGTH_FOR_I2_FIELD - result.length()));

		return result.toString();
	}

	/**
	 * Encrypt data.
	 *
	 * @param encryptKey the encrypt key
	 * @param data the data
	 * @return the string
	 */
	public static String encryptData(final String encryptKey, final String data) {
		return EncryptionUtil.getInstance(encryptKey).encryptData(data);
	}

}
