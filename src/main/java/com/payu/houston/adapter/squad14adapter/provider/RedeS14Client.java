package com.payu.houston.adapter.squad14adapter.provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RedeS14Client {

	/**Class logger*/
	private  static  final Logger LOGGER = LoggerFactory.getLogger(RedeS14Client.class);

	/** Network Hystrixs commnad name */
	private static  final String NETWOK_COMMAND_NAME="REDES14";

	/** Constant value for connection timeout*/
	public static final String CONNECTION_TIMEOUT="";

	/** Constant value for request timeout*/
	public  static  final String REQUEST_TIMEOUT="";

	/** value of Authorizaton url*/
	private String authorizathionUrl="";

	/**value of confirmation url*/
	private String confirmationUrl="";



}
