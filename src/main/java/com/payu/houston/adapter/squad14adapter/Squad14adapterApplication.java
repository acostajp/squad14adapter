package com.payu.houston.adapter.squad14adapter;

import com.payu.houston.adapter.HoustonAdapterApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;


@EnableHystrix
@EnableCircuitBreaker
@SpringBootApplication
public class Squad14adapterApplication extends HoustonAdapterApplication {

	public static void main(String[] args) {

		SpringApplication.run(Squad14adapterApplication.class, args);
	}

}
